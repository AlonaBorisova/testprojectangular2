import { Component, OnInit } from '@angular/core';
import { Navs } from '../../interfaces';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  public navs: Navs [] = [
    {
      url : '',
      content: 'Home'
    },
    {
      url : 'photos',
      content: 'Themes'
    },
    {
      url : 'search',
      content: 'Search'
    },
  ];
  constructor() { }

  ngOnInit() {
  }

}
