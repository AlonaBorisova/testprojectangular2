import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-footer',
  template: `
    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-offset-3 col-md-6">
            <p class="text-muted">{{ message }}</p>
          </div>
        </div>
      </div>
    </footer>
  `,
  styles: [`
  .footer {
    width: 100%;
    margin-top: 40px;
    height: 60px;
    padding: 10px;
    background-color: lightgray;
  }
`]
})
export class FooterComponent implements OnInit {
  private message = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, labore et dolore magna aliqua.';
  constructor() { }

  ngOnInit() {
  }

}
