import { HomeComponent } from '../../components/home/home.component';
import { ThemesComponent } from '../../components/themes/themes.component';
import { PhotoComponent } from '../../components/photo/photo.component';
import { SearchComponent } from '../../components/search/search.component';
import { RouterModule } from '@angular/router';

const routes = [
  {path: '', component: HomeComponent},
  {path: 'photos', component: ThemesComponent},
  {path: 'photos/:id', component: PhotoComponent},
  {path: 'search', component: SearchComponent},
  {path: 'lazyload', loadChildren: '../../lazyload/lazyload.module'}
];


export default RouterModule.forRoot(routes);
