export interface Post {
  id: number;
  title: string;
  body: string;
}

export interface Quotation {
  quoteText: string;
  quoteAuthor: string
}

export interface Navs {
  url: string;
  content: string;
}
