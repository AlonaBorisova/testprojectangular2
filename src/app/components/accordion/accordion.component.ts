import { Component, OnInit } from '@angular/core';
import { PostService } from '../../services/post.service';
import { LogDebuggerService } from '../../services/log-debugger.service';
import { ConsoleService } from '../../services/console.service';
import { Post } from '../../interfaces';

@Component({
  selector: 'app-accordion',
  providers: [PostService, ConsoleService,
    {
      provide: LogDebuggerService,
      useFactory: (consoleService) => {
        return new LogDebuggerService(consoleService, true);
      },
      deps: [ConsoleService]
    },
    {
      provide: 'apiUrl',
      useValue: 'https://jsonplaceholder.typicode.com/posts',
    }
  ],
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.css']
})
export class AccordionComponent implements OnInit {
  public posts: Post[] = [];
  private errorMessage:any = '';
  public highlight: string = 'highlight';
  public status: any = {
    isFirstOpen: true,
    isOpen: false,
    isHighlighted: true,
  };
  constructor(private postService:PostService, private logDebugger: LogDebuggerService) { }

  ngOnInit() {
    this.postService.getPosts()
      .subscribe(
        (posts) => {
          this.posts = posts;
          this.logDebugger.debug('we`ve got post from PostService where ConsoleService was injected through deps []');
        },
        error => this.errorMessage = <any>error);
  }

  removeAccordionGroup(index, id) {
    this.postService.removePostById(id).subscribe(
      () => {
        this.posts.splice(index, 1);
        this.logDebugger.debug(`remove one AccordionGroup with id=${id}`);
      },
      error => this.errorMessage = <any>error);
  }

  addAccordionGroup() {
    let accordionGroupContent = {
      id: this._randomIndex(100, 10),
      title:'New Content Added',
      body:'Content angular 2 accordion'
    };

    this.postService.addPost(accordionGroupContent).subscribe(
      (post) =>  {
        this.posts.splice(0,0, accordionGroupContent);
        this.logDebugger.debug(`added one AccordionGroup with id=${post.id}`);
      },
          error => this.errorMessage = <any>error
      );
  }

  _randomIndex(max, min): number {
    return Math.floor(Math.random() * (max - min) + min);
  }

}
