import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styles: [`
      carousel img {
      max-width: 100%;
      height: 300px;
      width: 860px;
      }
  `]
})
export class CarouselComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
