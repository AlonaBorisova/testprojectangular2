import {Component, OnInit} from '@angular/core';
import { PhotoService } from '../../services/photo.service';

@Component({
  selector: 'app-photos',
  providers: [PhotoService,
    {
      provide: 'apiUrl',
      useValue: 'https://jsonplaceholder.typicode.com/',
    }
  ],
  templateUrl: './themes.component.html',
  styleUrls: ['./themes.component.css']
})
export class ThemesComponent implements OnInit {
  public themes: Array<any> = [];
  private errorMessage:any = '';

  constructor(private photoService: PhotoService) {
  }

  ngOnInit() {
    this.photoService.getThemes()
      .subscribe(
        (themes) => {
          this.themes = themes;
        },
        error => this.errorMessage = <any>error);
  }

}
