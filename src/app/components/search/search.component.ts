import { Component, OnInit } from '@angular/core';
import { SearchService } from '../../services/search.service';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-search',
  providers: [SearchService],
  templateUrl: './search.component.html',
  styles: [`
    .wrapper{
      height: 815px;
    }
  `]
})
export class SearchComponent implements OnInit {
  items:Array<string>;
  term$ = new Subject<string>();
  constructor(private service:SearchService) { }

  ngOnInit() {
    this.service.search(this.term$)
      .subscribe((results) => {
        this.items = results;
      });
  }

}
