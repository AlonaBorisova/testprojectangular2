import { Component, OnInit } from '@angular/core';
import {PhotoService} from '../../services/photo.service';

@Component({
  selector: 'app-photo',
  providers: [PhotoService,
    {
      provide: 'apiUrl',
      useValue: 'https://jsonplaceholder.typicode.com/',
    }
  ],
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.css']
})
export class PhotoComponent implements OnInit {
  private errorMessage: any = '';
  public photo$: any = {};
  constructor(private photoService: PhotoService ) {
  }

  ngOnInit() {
    this.photoService.getPhoto()
      .subscribe(
        (photo) => {
          this.photo$ = photo;
        },
        error => this.errorMessage = <any>error);
  }

}
