import { Injectable } from '@angular/core';

@Injectable()
export class LogDebuggerService {

  constructor(private consoleService, private enabled: boolean) { }

  debug(message) {
    if (this.enabled) {
      this.consoleService.log(`DEBUG: ${message}`);
    }
  }

}
