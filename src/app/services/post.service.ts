import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";
import { Inject } from '@angular/core';
import 'rxjs/add/operator/catch';

@Injectable()
export class PostService {

  constructor(private http: Http,  @Inject('apiUrl') private apiUrl) { }

  getPosts(){
    const apiUrl = `${this.apiUrl}?userId=1`;
    return this.http.get(apiUrl).map(res => res.json()).catch(this.handleError);
  }

  removePostById(id) {
    const api = `${this.apiUrl}/${id}`;
    return this.http.delete(api).map(res => res.json()).catch(this.handleError);
  }

  addPost(data) {
    return this.http.post(this.apiUrl, data).map(res => res.json()).catch(this.handleError);
  }

  private handleError(error:any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
