import { Injectable } from '@angular/core';
import {Jsonp, Headers, RequestOptions, Http} from "@angular/http";
import "rxjs/add/operator/map";
import "rxjs/add/operator/do";
import "rxjs/add/operator/switchMap";
import {Observable} from "rxjs/Observable";
import { Subject } from 'rxjs/Subject';
import { Quotation } from '../interfaces';

@Injectable()
export class QuotationService {
   private quotation: Quotation = {quoteText: '', quoteAuthor: ''};
   private subject = new Subject<any>();
  constructor(private http: Http) {
  }

  getQuatation() {
    return this.quotation;
  }

  performRequest(){
    // let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded','Accept': 'application/json' });
    // const api = 'http://api.forismatic.com/api/1.0/?method=getQuote&lang=ru&format=json';
    const api = 'https://jsonplaceholder.typicode.com/users';
    return this.http.get(api).map(res => res.json()).catch(this.handleError);
  }

  sendMessage(message: string) {
    this.subject.next({ text: message });
  }
  clearMessage() {
    this.subject.next();
  }


  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

  private handleError(error:any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.log(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

}
