import {Inject, Injectable} from '@angular/core';
import {Http} from "@angular/http";
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/switchMap";
import 'rxjs/add/operator/catch';

@Injectable()
export class PhotoService {

  constructor(private route:ActivatedRoute, private http: Http, @Inject('apiUrl') private apiUrl) { }

  getThemes(){
    const apiUrl = `${this.apiUrl}photos?albumId=1`;
    return this.http.get(apiUrl).map(res => res.json()).catch(this.handleError);
  }

  getPhoto() {
    return this.route.params
      .map((p:any) => p.id)
      .switchMap(id => this.http.get(this.apiUrl + 'photos/' + id)
        .map(res => res.json())
        .map(photo => Object.assign({}, photo))
      ).catch(this.handleError);
  }

  private handleError(error:any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
