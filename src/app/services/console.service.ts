import { Injectable } from '@angular/core';

@Injectable()
export class ConsoleService {

  constructor() { }

  log(message) {
    console.log(message);
  }

}
