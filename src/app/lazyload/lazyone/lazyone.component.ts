import { Component, OnInit } from '@angular/core';
import { QuotationService } from '../../services/quotation.service';

@Component({
  selector: 'app-lazyone',
  providers: [QuotationService],
  templateUrl: './lazyone.component.html',
  styleUrls: ['./lazyone.component.css']
})
export class LazyoneComponent implements OnInit {
  public users: any[] = [];
  public detailInfo: any = {};
  constructor(private quotation: QuotationService) { }

  ngOnInit() {
    this.quotation.performRequest()
      .subscribe(message => { this.users = message; });
  }

  getDetailInfo(id) {
    this.detailInfo = Object.assign({}, this.users[id]);
  }

}
