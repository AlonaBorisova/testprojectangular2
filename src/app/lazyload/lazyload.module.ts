import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { LazyoneComponent } from './lazyone/lazyone.component';
import lazyRoute from './lazyone/lazyone.routes';
import { DetailComponent } from './detail/detail.component';


@NgModule({
  imports:[CommonModule, lazyRoute],
  declarations: [LazyoneComponent, DetailComponent],
  exports: [ LazyoneComponent ],
})

export default class LazyLoadModule{}
