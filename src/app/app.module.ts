import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { CarouselModule, AccordionModule, PaginationModule } from 'ngx-bootstrap';
import appRoutes from './common/routes/routes';

import { AppComponent } from './app.component';
import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { PhotoComponent } from './components/photo/photo.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { ThemesComponent } from './components/themes/themes.component';
import { SearchComponent } from './components/search/search.component';
import { AccordionComponent } from './components/accordion/accordion.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    CarouselModule.forRoot(),
    AccordionModule.forRoot(),
    PaginationModule.forRoot(),
    appRoutes,
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    PhotoComponent,
    CarouselComponent,
    ThemesComponent,
    SearchComponent,
    AccordionComponent,
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
