import { TestProjectAngular2Page } from './app.po';

describe('test-project-angular2 App', function() {
  let page: TestProjectAngular2Page;

  beforeEach(() => {
    page = new TestProjectAngular2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
